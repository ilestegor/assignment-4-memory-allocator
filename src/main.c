//
// Created by Егор Глотов on 17.12.2023.
//
#include "tests/test.h"

int main() {
    run_tests("Basic memory allocation test started...\n", basic_memory_allocation);
    run_tests("Release one of allocated blocks test started...\n", release_one_of_allocated_blocks);
    run_tests("Release two block from many allocated started...\n", release_two_of_allocated_blocks);
    run_tests("Memory expansion on region test started...\n", grow_memory_test);
    run_tests("Allocation in a new region started...\n", allocate_new_region);
    return 0;
}