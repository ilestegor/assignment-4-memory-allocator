//
// Created by Егор Глотов on 23.12.2023.
//

#ifndef MEMORY_ALLOCATOR_TEST_H
#define MEMORY_ALLOCATOR_TEST_H

void basic_memory_allocation(void *heap);

void release_one_of_allocated_blocks(void *heap);

void release_two_of_allocated_blocks(void *heap);

void grow_memory_test(void *heap);

void allocate_new_region(void *heap);

void *init_heap_debug(const char *test_name);

void run_tests(const char *test_name, void (*test_function)());

#endif //MEMORY_ALLOCATOR_TEST_H
